SHELL = /bin/bash
DESTDIR ?= build
SOURCES := $(shell find libkeyringctl -name '*.py' -or -type d) keyringctl pyproject.toml

all: build

lint:
	black --check --diff keyringctl libkeyringctl tests
	isort --diff .
	flake8 keyringctl libkeyringctl tests
	mypy --install-types --non-interactive keyringctl libkeyringctl tests

fmt:
	black .
	isort .

test:
	coverage run
	coverage xml
	coverage report --fail-under=100.0

build: $(SOURCES)
	python -m build --wheel --no-isolation

clean:
	rm -rf dist build

install: build
	python -m installer --destdir="$(DESTDIR)" dist/*.whl

.PHONY: all lint fmt test build clean install
